


$(document).ready(function() {
  var targetDate = new Date("2023-06-16"); // Specify the target date

  // Function to compare dates
  function isTargetDate(date) {
    return (
      date.getFullYear() === targetDate.getFullYear() &&
      date.getMonth() === targetDate.getMonth() &&
      date.getDate() === targetDate.getDate()
    );
  }

  // Function to hide elements
  function hideElementsOnTargetDate() {
    var currentDate = new Date();
    if (isTargetDate(currentDate)) {
      // Add the selectors of the elements you want to hide
      $(".row").hide();
      $(".container").hide();
      $(".card").hide();
      $(".btn").hide();
      $(".text").hide();
      $(".col-md-6").hide();
    }
  }

  // Call the function on page load
  hideElementsOnTargetDate();
});
