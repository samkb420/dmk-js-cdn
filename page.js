
        $(document).ready(function() {
            // Initialize DataTables with pagination options and export buttons
            $('#myTable').DataTable({
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "buttons": [
                    'csv', 'excel'
                ]
            });
        });
    
